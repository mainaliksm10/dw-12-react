import React, { createContext, useState } from "react";
import MyNavbar from "./mycomponent/MyNavbar";
import MyRoutes from "./mycomponent/MyRoutes";
import FormikForm from "./mycomponent/LearnFormik/FormikForm";
import FormikTutorial from "./mycomponent/LearnFormik/FormikTutorial";
import ReactRouter from "./mycomponent/ReactRouter";
import LearnUseRef1 from "./component/learnUseRef/LearnUseRef1";

export let GlobalVariableContext = createContext();

const MyApp = () => {
  let [token, setToken] = useState(localStorage.getItem("token"));
  return (
    <div>
      <GlobalVariableContext.Provider
        value={{ token: token, setToken: setToken }}
      >
        {/* <MyNavbar></MyNavbar>
      <MyRoutes></MyRoutes> */}
        {/* <FormikForm></FormikForm> */}
        {/* <FormikTutorial></FormikTutorial> */}
        <ReactRouter></ReactRouter>
        {/* <LearnUseRef1></LearnUseRef1> */}
      </GlobalVariableContext.Provider>
    </div>
  );
};

export default MyApp;
