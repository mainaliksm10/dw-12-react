let fun1 = (a, b, ...c) => {
  console.log("a", a);
  console.log("b", b);
  console.log("c", c);
};
fun1(1, 2, 3, 4, 5);

let [details, ...other] = ["kamalsheel", 22, false];
console.log(details);
console.log(other);

let {favDestination, ...others} = {
  favAnime: "One Piece",
  favSports: "Football",
  favDestination: "Bali",
};

console.log(favDestination);
console.log(others);
