import React from "react";

const EffectOfData = () => {
  let name = "kamalsheel";
  let age = 22;
  let isMarried = false;
  let tags = [<div>Hello1</div>, <div>Hello2</div>, <div>Hello3</div>];

  let info = { name: "kamalsheel", age: 22 };
  return (
    <div>
      <p>{name}</p>
      <p>{age}</p>
      <p>{isMarried}</p>
      <p>{tags}</p>
      {/* <p>{info}</p> => It is not possible */}
      <p>{info.name}</p>
      <p>{info.age}</p>
    </div>
  );
};

export default EffectOfData;

// boolean are not displayed in browser
// when array is placed in the browser => it place element one by one (note [] and, are not written in browser)
// we cannot place object in browser (Error : object is not valid as react child)
