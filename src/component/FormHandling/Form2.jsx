import React, { useState } from "react";

const Form2 = () => {
  let [fullName, setFullName] = useState("");
  let [address, setAddress] = useState("");
  let [email, setEmail] = useState("");
  let [description, setDescription] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [isDestination, setIsDestination] = useState("nepal");
  let [gender, setGender] = useState("male");

  let handleSubmit = (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      address: address,
      email: email,
      description: description,
      isMarried: isMarried,
      isDestination: isDestination,
      gender: gender,
    };
    console.log("Data has been submitted!", data);
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="fullName">Full Name: </label>
          <input
            id="fullName"
            type="text"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="address">Address: </label>
          <input
            id="address"
            type="text"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="email">E-mail: </label>
          <input
            id="email"
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="description">Description: </label>
          <textarea
            id="description"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          ></textarea>
        </div>

        <div>
          <label htmlFor="isMarried">IsMarried: </label>
          <input
            type="checkbox"
            id="isMarried"
            checked={isMarried}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          ></input>
        </div>

        <div>
          <label>Destination: </label>
          <select
            value={isDestination}
            onChange={(e) => {
              setIsDestination(e.target.value);
            }}
          >
            <option value="nepal">Nepal</option>
            <option value="india">India</option>
            <option value="china">China</option>

            {/* {isDestination.map()} */}
          </select>
        </div>

        <div>
          <label htmlFor="male">Gender: </label>
          {/* <label htmlFor="male">Male</label>
          <input
            type="radio"
            id="male"
            value="male"
            checked={gender === "male"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>
          <label htmlFor="female">Female</label>
          <input
            type="radio"
            id="female"
            value="female"
            checked={gender === "female"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input>
          <label htmlFor="other">Other</label>
          <input
            type="radio"
            id="other"
            value="other"
            checked={gender === "other"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          ></input> */}

          {genders.map((item, i) => {
            return (
              <span>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  id={item.value}
                  value={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </span>
            );
          })}
        </div>

        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default Form2;

/* 
    input           value          e.target.value 
    all             value          e.target.value i 
    checkbox        checked        e.target.checked 
    radio           checked        e.target.value
*/
