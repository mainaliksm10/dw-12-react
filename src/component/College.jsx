import React from 'react'

const College = (props) => {
  return (
    <div>
        <p>Name:{props.name}</p>
        <p>Address:{props.address}</p>
        <p>Cost:{props.cost}</p>
    </div>
  )
}

export default College