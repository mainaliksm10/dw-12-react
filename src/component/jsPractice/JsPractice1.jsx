import React from "react";

const JsPractice1 = () => {
  let products = [
    {
      id: 1,
      title: "MacBook Pro",
      category: "Laptops",
      price: 100000.0,
      quantity: 2,
      description: "A high-performance laptop.",
      manufactureDate: "2023-05-15T08:30:00",
      isAvailable: true,
    },
    {
      id: 2,
      title: "Nike",
      category: "Running Shoes",
      price: 5000,
      quantity: 3,
      description: "Running shoes designed for speed and comfort.",
      manufactureDate: "2023-02-20T14:45:00",
      isAvailable: true,
    },
    {
      id: 3,
      title: "Python",
      category: "Books",
      price: 500,
      quantity: 1,
      description: "A language for AI",
      manufactureDate: "1925-04-10T10:10:00",
      isAvailable: false,
    },
    {
      id: 4,
      title: "Javascript",
      category: "Books",
      price: 700,
      quantity: 5,
      description: "A language for Browser",
      manufactureDate: "1995-12-04T12:00:00",
      isAvailable: false,
    },
    {
      id: 5,
      title: "Dell XPS",
      category: "Laptops",
      price: 120000.0,
      quantity: 2,
      description: "An ultra-slim laptop with powerful performance.",
      manufactureDate: "2023-04-25T09:15:00",
      isAvailable: true,
    },
  ];

  let task1 = () => {
    let output = products.map((val, i) => {
      return <div key={i}>{val.title}</div>;
    });
    return output;
  };

  let task2 = () => {
    let output = products.map((val, i) => {
      return (
        <div>
          {val.title} costs {val.price} and it belongs to {val.category}
        </div>
      );
    });
    return output;
  };

  let task3 = () => {
    let output = products
      .filter((val, i) => {
        if (val.price > 2000) {
          return <div>{val.price}</div>;
        }
      })
      .map((val, i) => {
        return (
          <div>
            {val.title} costs NRS.{val.price} and its category is {val.category}
          </div>
        );
      });
    return output;
  };

  let task4 = () => {
    let output = products
      .filter((val, i) => {
        if (val.category === "Books") {
          return <div>{val.category}</div>;
        }
      })
      .map((val, i) => {
        return (
          <div>
            {val.title} costs NRS.{val.price} whose category is {val.category}
          </div>
        );
      });
    return output;
  };

  let task5 = () => {
    let output = products.reduce((prev, cv) => {
      return prev + cv.price;
    }, 0);
    return output;
  };

  let task6 = () => {

    let categories = products.map((val) => val.category);

 
    let uniqueCategories = [...new Set(categories)];

    return uniqueCategories;
  };

  

  return (
    <div>
      JsPractice1
      <h1>The product in our shop are:</h1>
      <br></br>
      {/* {task1()} */}
      {/* {task2()} */}
      {/* {task3()} */}
      {/* {task4()} */}
      <p>The total price of all product is NRS.{task5()}</p>
      {task6()}
      <br></br>
    </div>
  );
};

export default JsPractice1;
