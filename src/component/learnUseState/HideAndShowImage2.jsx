import React, { useState } from "react";

const HideAndShowImage2 = () => {
  let [show, setShow] = useState(true);

  let handleToggle = () => {
    setShow(!show);
  };
  return (
    <>
      {show ? <img src="/favicon.ico"></img> : null}
      <br></br>
      <button
        // onClick={() => {
        //   setShow(!show);
        // }
        //or}
        onClick={handleToggle}
      >
        Show And Hide
      </button>
    </>
  );
};

export default HideAndShowImage2;
