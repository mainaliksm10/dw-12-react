import React, { useState } from "react";

const LearnUseState = () => {
  let [age, setAge] = useState(22);
  let [name, setName] = useState("Kamalsheel");
  return (
    <div>
      {age} <br></br>
      {name} <br></br>
      <button
        onClick={() => {
          setAge(99);
          setName("Kamalsheel Mainali");
        }}
      >
        Click Me
      </button>
    </div>
  );
};

export default LearnUseState;
