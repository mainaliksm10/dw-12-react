import React, { useState } from "react";

const HideAndShowImage = () => {
  let [show, setShow] = useState(true);
  let handleShow = () => {
    setShow(true);
  };

  let hideShow = () => {
    setShow(false);
  };
  return (
    <div>
      {show ? <img src="/favicon.ico"></img> : null}
      <br></br>
      <button 
      // onClick={() => {
        //   setShow(true));
        // }
      onClick={handleShow}>Show</button>
      <br></br>
      <button
      // onClick={() => {
        //   setShow(false);
        // }
      onClick={hideShow}>Hide</button>
    </div>
  );
};

export default HideAndShowImage;
