import React, { useState } from "react";

const UseState2 = () => {
  let [count1, setCount1] = useState(10);
  return (
    <>
      <p>*****************Count 1 is {count1}</p>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Click here to increase
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 - 1);
        }}
      >
        Click here to decrease{" "}
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount1((count1 = 0));
        }}
      >
        Click here to reset
      </button>
    </>
  );
};

export default UseState2;
