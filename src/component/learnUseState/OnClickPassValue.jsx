import React from "react";

const OnClickPassValue = () => {
  let handleClick1 = () => {
    console.log("Button 1 is clicked");
  };

  let handleClick2 = (a, b) => {
    console.log("Button 2 is clicked");
  };

  return (
    <div>
      <br></br>
      <button onClick={handleClick1}>Click me1</button>
      <button onClick={handleClick2(1, 2)}>Click me2</button>
      {/* The above technique is used if we need to pass value */}
    </div>
  );
};

export default OnClickPassValue;
