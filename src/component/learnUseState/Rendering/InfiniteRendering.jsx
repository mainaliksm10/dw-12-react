import React, { useState } from "react";

const InfiniteRendering = () => {
  let [count1, setCount1] = useState(0);

  // setCount1(count1 + 1);

  // line no 6 results in infinite rendering

  // to prevent infinite rendering
  // always place setCount1 on event (button click ,....) or inside useEffect
  return (
    <div>
      <p>The value of count1 is {count1}</p>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Click to increase the count
      </button>
    </div>
  );
};

export default InfiniteRendering;
