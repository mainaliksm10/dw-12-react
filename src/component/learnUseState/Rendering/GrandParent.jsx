import React, { useState } from "react";
import Parent from "./Parent";

const GrandParent = () => {
  console.log("GrandParent component gets render.");
  let [count1, setCount1] = useState(0);
  return (
    <div>
      <p>The value of count1 is:{count1}</p>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1 by 1
      </button>
      <Parent count1={count1}></Parent>
    </div>
  );
};

export default GrandParent;
