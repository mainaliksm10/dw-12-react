import React, { useState } from "react";

const Rendering1 = () => {
  let [count1, setCount1] = useState(0);
  let [count2, setCount2] = useState(100);
  return (
    <div>
      <p>The value of count1 is: {count1}</p>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <br></br>
      <p>The value of count2 is: {count2}</p>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        Increment count2
      </button>
    </div>
  );
};

export default Rendering1;

/* 
    1. When state variable changes a component will render (execute)

    2. Component will only render if state variable changes, if it does not component will not render

    3. When a component is render by count1 state variable ( the state variable will be updated) (other state variable such as count2 ,... will store previous value)
    
*/
