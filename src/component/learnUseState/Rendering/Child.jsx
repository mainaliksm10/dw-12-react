import React from "react";

const Child = ({ count1 }) => {
  console.log("Child component gets render.");

  return (
    <div>
      <p>Hello, I am nested.</p>
      <p>The value of count is {count1}</p>
    </div>
  );
};

export default Child;
