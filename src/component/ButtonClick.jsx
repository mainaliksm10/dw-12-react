import React from "react";

const ButtonClick = () => {
  return (
    <div>
      <button
        onClick={() => {
          console.log("Button has been clicked");
        }}
      >
        Click Here
      </button>
    </div>
  );
};

export default ButtonClick;
