import React from "react";
import { useParams } from "react-router-dom";

const GetDynamicRouteParameter = () => {
  const params = useParams();
  return (
    <div>
      GetDynamicRouteParameter
      <br></br>
      {params.id1}
      <br></br>
      {params.id2}
      <br></br>
    </div>
  );
};

export default GetDynamicRouteParameter;
