import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";

const NestingRoute = () => {
  return (
    <div>
      {/* give slash(/) to home
      don't give / to other */}
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route
            path="student"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<div>Student's page </div>}></Route>
            <Route path="1" element={<div>1</div>}></Route>
            <Route path="Kamalsheel" element={<div>Kamalsheel</div>}></Route>
          </Route>
          <Route path="*" element={<div>404 Page</div>}></Route>
          <Route index element={<div>Home Page</div>}></Route>
        </Route>
      </Routes>
    </div>
  );
};

export default NestingRoute;
