import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

const About = () => {
  // const [searchParams] = useSearchParams();
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  // http://localhost:3000/about?name=Kamalsheel&age=22&address=chabahil
  return (
    <div>
      About
      <br />
      name is {searchParams.get("name")}
      <br />
      age is {searchParams.get("age")}
      <br />
      address is {searchParams.get("address")}
      <br></br>
      <br></br>
      <button
        onClick={() => {
          // navigate("/contact");
          navigate("/contact", { replace: true });
        }}
      >
        go to contact page
      </button>
    </div>
  );
};

export default About;
