import React from "react";
import { NavLink, Outlet, Route, Routes } from "react-router-dom";
import About from "./learnRoutes/About";
import Contact from "./learnRoutes/Contact";
import Error from "./learnRoutes/Error";
import Home from "./learnRoutes/Home";
import GetDynamicRouteParameter from "./GetDynamicRouteParameter";

const LearnRout = () => {
  return (
    <div>
      <NavLink to="/" style={{ margin: "22px" }}>
        Home
      </NavLink>
      <NavLink to="/about" style={{ margin: "22px" }}>
        About
      </NavLink>
      <NavLink to="/contact" style={{ margin: "22px" }}>
        Contact
      </NavLink>

      <br />

      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/about" element={<About></About>}></Route>
        <Route path="/contact" element={<Contact></Contact>}></Route>
        <Route path="*" element={<Error></Error>}></Route>
        <Route path="/a" element={<div>a page</div>}></Route>
        {/* <Route path="/a/a1" element={<div>a1 page</div>}></Route>
        <Route path="/a/a1/a2" element={<div>a2 page</div>}></Route>
        <Route path="/a/:any" element={<div>Any page</div>}></Route> */}
        <Route
          path="/b/:id1/id/:id2"
          element={<GetDynamicRouteParameter></GetDynamicRouteParameter>}
        ></Route>
        <Route
          path="a"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route
            path="a1"
            element={
              <div>
                a a1 <Outlet></Outlet>
              </div>
            }
          >
            <Route path="a2" element={<div>a2 page</div>}></Route>
          </Route>
          <Route path=":any" element={<div>Any page</div>}></Route>
        </Route>
      </Routes>
    </div>
  );
};

export default LearnRout;
