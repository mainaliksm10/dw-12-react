import React from 'react'

// const Info = (props) => {
//   return (
//     <div>
//         <p>My name is {props.name} and my age is {props.age}. I live in {props.address}.</p>
//     </div>
//   )
// }

const Info = ({name,address,age})=>{
    return (
        <div>
            <p>My name is {name} and my age is {age}. I live in {address}</p>
        </div>
    )
}
export default Info