import React from "react";
import LearnRout from "./component/learnRouting/LearnRout";
import ButtonClick from "./component/ButtonClick";
import TernaryOperator from "./component/TernaryOperator";
import EffectOfData from "./component/EffectOfData";
import Info from "./component/Info";
import College from "./component/College";
import Style from "./component/Style";
import LearnUseState from "./component/learnUseState/LearnUseState";
import NestingRoute from "./component/learnRouting/learnRoutes/NestingRoute";
import JsPractice1 from "./component/jsPractice/JsPractice1";
import UseState2 from "./component/learnUseState/UseState2";
import HideAndShowImage from "./component/learnUseState/HideAndShowImage";
import HideAndShowImage2 from "./component/learnUseState/HideAndShowImage2";
import OnClickPassValue from "./component/learnUseState/OnClickPassValue";
import Rendering1 from "./component/learnUseState/Rendering/Rendering1";
import GrandParent from "./component/learnUseState/Rendering/GrandParent";
import InfiniteRendering from "./component/learnUseState/Rendering/InfiniteRendering";
import Form1 from "./component/FormHandling/Form1";
import Form2 from "./component/FormHandling/Form2";

const App = () => {
  return (
    <div>
      <Info name="kamalsheel" age={22} address="chabahil"></Info>
      <College
        name="Asian College of Higher Studies"
        address="Ekantakuna"
        cost={860000}
      ></College>
      <Style></Style>
      <EffectOfData></EffectOfData>
      <TernaryOperator></TernaryOperator>
      <ButtonClick></ButtonClick>
      <LearnUseState></LearnUseState>
      <br></br>
      {/* <LearnRout></LearnRout> */}
      <br></br>
      {/* <NestingRoute></NestingRoute> */}
      {/* <JsPractice1></JsPractice1> */}
      <br></br>
      {/* <UseState2></UseState2> */}
      <br></br>
      <br></br>
      {/* <HideAndShowImage></HideAndShowImage> */}
      <br></br>
      <HideAndShowImage2></HideAndShowImage2>
      <br></br>
      <OnClickPassValue></OnClickPassValue>
      <br></br>
      <Rendering1></Rendering1>
      <br></br>
      <GrandParent></GrandParent>
      <br></br>
      <InfiniteRendering></InfiniteRendering>
      <br></br>
      <Form1></Form1>
      <br></br>
      <Form2></Form2>
    </div>
  );
};

export default App;
