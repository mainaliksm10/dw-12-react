import React from "react";
import { NavLink } from "react-router-dom";

const MyNavbar = () => {
  return (
    <div>
      <NavLink
        to="/teachers"
        style={{ marginRight: "30px", textDecoration: "None" }}
      >
        Teacher
      </NavLink>
      <NavLink
        to="/teachers/create"
        style={{ marginRight: "30px", textDecoration: "None" }}
      >
        Create Teacher
      </NavLink>
    </div>
  );
};

export default MyNavbar;
