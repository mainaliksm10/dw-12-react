import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { GlobalVariableContext } from "../MyApp";

const ReactLink = () => {
  let global = useContext(GlobalVariableContext);
  console.log(global);
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        gap: "40px",
        backgroundColor: "black",
        marginBottom: "30px",
        fontSize: "3vh",
      }}
    >
      <NavLink
        to="/admin/register"
        style={{
          marginRight: "20px",
          textDecoration: "none",
          color: "#fff",
          fontSize: "3vh",
        }}
      >
        Admin Register
      </NavLink>

      {global.token ? (
        <>
          <NavLink
            to="/admin/my-profile"
            style={{
              marginRight: "20px",
              textDecoration: "none",
              color: "#fff",
              fontSize: "3vh",
            }}
          >
            My Profile
          </NavLink>
          <NavLink
            to="/admin/logout"
            style={{
              marginRight: "20px",
              textDecoration: "none",
              color: "#fff",
              fontSize: "3vh",
            }}
          >
            LogOut
          </NavLink>
          <NavLink
            to="/admin/update-password"
            style={{
              marginRight: "20px",
              textDecoration: "none",
              color: "#fff",
              fontSize: "3vh",
            }}
          >
            Update Password
          </NavLink>

          <NavLink
            to="/admin/read-all-user"
            style={{
              marginRight: "20px",
              textDecoration: "none",
              color: "#fff",
              fontSize: "3vh",
            }}
          >
            Read All User
          </NavLink>
        </>
      ) : (
        <>
          <NavLink
            to="/admin/login"
            style={{
              marginRight: "20px",
              textDecoration: "none",
              color: "#fff",
              fontSize: "3vh",
            }}
          >
            Admin login
          </NavLink>
        </>
      )}
    </div>
  );
};

export default ReactLink;
