import React, { useContext } from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import ReactLink from "./ReactLink";
import AdminForgotPassword from "./WebUsers/AdminForgotPassword";
import AdminLogin from "./WebUsers/AdminLogin";
import AdminLogout from "./WebUsers/AdminLogout";
import AdminProfile from "./WebUsers/AdminProfile";
import AdminProfileUpdate from "./WebUsers/AdminProfileUpdate";
import AdminRegister from "./WebUsers/AdminRegister";
import AdminResetPassword from "./WebUsers/AdminResetPassword";
import AdminUpdatePassword from "./WebUsers/AdminUpdatePassword";
import AdminVerify from "./WebUsers/AdminVerify";
import ReadAllUser from "./WebUsers/ReadAllUser";
import ReadSpecificUser from "./WebUsers/ReadSpecificUser";
import UpdateSpecificUser from "./WebUsers/UpdateSpecificUser";
import { GlobalVariableContext } from "../MyApp";

const ReactRouter = () => {
  let { token, setToken } = useContext(GlobalVariableContext);

  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <ReactLink></ReactLink>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>

          <Route
            path="verify-email"
            // path="reset-password"
            element={<AdminVerify></AdminVerify>}
          ></Route>
          <Route
            path="reset-password"
            element={<AdminResetPassword></AdminResetPassword>}
          ></Route>

          <Route
            path="admin"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            {token ? (
              <>
                <Route
                  path="update-password"
                  element={<AdminUpdatePassword></AdminUpdatePassword>}
                ></Route>

                <Route
                  path="profile-update"
                  element={<AdminProfileUpdate></AdminProfileUpdate>}
                ></Route>

                <Route
                  path="logout"
                  element={<AdminLogout></AdminLogout>}
                ></Route>

                <Route
                  path="my-profile"
                  element={<AdminProfile></AdminProfile>}
                ></Route>

                <Route
                  index
                  element={<div>This is admin dashboard</div>}
                ></Route>

                <Route
                  path=":id"
                  element={<ReadSpecificUser></ReadSpecificUser>}
                ></Route>

                <Route path="update" element={<Outlet></Outlet>}>
                  <Route
                    path=":id"
                    element={<UpdateSpecificUser></UpdateSpecificUser>}
                  ></Route>
                </Route>

                <Route
                  path="read-all-user"
                  element={<ReadAllUser></ReadAllUser>}
                ></Route>
              </>
            ) : (
              <>
                <Route path="login" element={<AdminLogin></AdminLogin>}></Route>
              </>
            )}
            <Route
              path="register"
              element={<AdminRegister></AdminRegister>}
            ></Route>

            <Route
              path="forgot-password"
              element={<AdminForgotPassword></AdminForgotPassword>}
            ></Route>

            <Route path="*" element={<div>404 Page</div>}></Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default ReactRouter;
