import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const AdminProfileUpdate = () => {
  let [fullName, setFullName] = useState("");
  let [dob, setDob] = useState("");
  let [phoneNumber, setPhoneNumber] = useState("");
  let [gender, setGender] = useState("male");

  let navigate = useNavigate();

  const getAdminProfile = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/my-profile`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      let data = result.data.data;

      setDob(data.dob);
      setFullName(data.fullName);
      setGender(data.gender);
      setPhoneNumber(data.phoneNumber);
    } catch (error) {}
  };

  useEffect(() => {
    getAdminProfile();
  }, []);

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      phoneNumber: phoneNumber,
      dob: dob,
      gender: gender,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/update-profile`,
        method: "patch",
        data: data,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      navigate("/admin/my-profile");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="fullName">Full Name: </label>
          <input
            id="fullName"
            type="text"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="dob">Date Of Birth: </label>
          <input
            id="dob"
            type="date"
            value={dob}
            onChange={(e) => {
              setDob(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="phoneNumber">phoneNumber: </label>
          <input
            id="phoneNumber"
            type="Number"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="male">Gender: </label>

          {genders.map((item, i) => {
            return (
              <span>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  id={item.value}
                  value={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </span>
            );
          })}
        </div>

        <button type="submit">Update</button>
      </form>
    </div>
  );
};

export default AdminProfileUpdate;
