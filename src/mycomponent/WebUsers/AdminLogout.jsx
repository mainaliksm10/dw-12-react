import React, { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { GlobalVariableContext } from "../../MyApp";

const AdminLogout = () => {
  let global = useContext(GlobalVariableContext);
  let navigate = useNavigate();

  localStorage.removeItem("token");
  global.setToken(null);

  useEffect(() => {
    navigate("/");
  }, []);
  return <div>AdminLogout</div>;
};

export default AdminLogout;
