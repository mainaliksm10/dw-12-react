import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const AdminForgotPassword = () => {
  let [email, setEmail] = useState("");

  let navigate = useNavigate();

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      email: email,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/forgot-password`,
        method: "post",
        data: data,
      });

      setEmail("");
      toast.success(
        "An email containing a password reset link has been sent to your inbox."
      );

      //   localStorage.removeItem("token");
      //   navigate("/admin/login");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="email">Email : </label>
          <input
            id="email"
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>

        <button style={{ marginTop: "12px" }} type="submit">
          Forgot Password
        </button>
      </form>
    </div>
  );
};

export default AdminForgotPassword;
