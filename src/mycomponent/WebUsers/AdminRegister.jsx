import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";

const AdminRegister = () => {
  let [fullName, setFullName] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [dob, setDob] = useState("");
  let [phoneNumber, setPhoneNumber] = useState("");
  let [gender, setGender] = useState("male");

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      phoneNumber: phoneNumber,
      email: email,
      password: password,
      dob: dob,
      gender: gender,
    };

    data = {
      ...data,
      role: "admin",
    };
    // console.log("Data has been submitted!", data);

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users`,
        method: "post",
        data: data,
      });

      toast.success(
        "A link has been sent to your email. Please click the given link to verify your account!"
      );
      setFullName("");
      setEmail("");
      setPassword("");
      setDob("");
      setPhoneNumber("");
      setGender("");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="fullName">Full Name: </label>
          <input
            id="fullName"
            type="text"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="password">Password: </label>
          <input
            id="password"
            type="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="dob">Date Of Birth: </label>
          <input
            id="dob"
            type="date"
            value={dob}
            onChange={(e) => {
              setDob(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="phoneNumber">phoneNumber: </label>
          <input
            id="phoneNumber"
            type="Number"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="email">E-mail: </label>
          <input
            id="email"
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="male">Gender: </label>

          {genders.map((item, i) => {
            return (
              <span>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  id={item.value}
                  value={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </span>
            );
          })}
        </div>

        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default AdminRegister;
