import axios from "axios";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { GlobalVariableContext } from "../../MyApp";

const AdminLogin = () => {
  let navigate = useNavigate();
  let global = useContext(GlobalVariableContext);

  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      email: email,
      password: password,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/login`,
        method: "POST",
        data: data,
      });

      let token = result.data.token;
      localStorage.setItem("token", token);
      global.setToken(token);

      navigate("/admin");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="email">E-mail: </label>
          <input
            id="email"
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="password">Password: </label>
          <input
            id="password"
            type="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>

        <button style={{ cursor: "pointer", marginTop: "15px" }} type="submit">
          Login
        </button>
        <div
          style={{ cursor: "pointer", marginTop: "5px" }}
          onClick={() => {
            navigate("/admin/forgot-password");
          }}
        >
          Forgot Password
        </div>
      </form>
    </div>
  );
};

export default AdminLogin;
