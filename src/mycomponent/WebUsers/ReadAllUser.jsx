import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ReadAllUser = () => {
  let [users, setUsers] = useState([]);
  let navigate = useNavigate();

  let getUsers = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/web-users",
        method: "get",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setUsers(result.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getUsers();
  }, []);
  return (
    <div>
      <ToastContainer />
      {users.map((item, i) => {
        return (
          <div
            key={i}
            style={{
              border: "solid 10px orange",
              marginBottom: "5px",
              paddingLeft: "5px",
            }}
          >
            <p>FullName is {item.fullName}</p>
            <p>Email is {item.email}</p>
            <p>Gender is {item.gender}</p>
            <p>Password is {item.password}</p>
            <button
              onClick={() => {
                navigate(`/admin/${item._id}`);
              }}
            >
              View
            </button>
            <button
              onClick={() => {
                navigate(`/admin/update/${item._id}`);
              }}
            >
              Edit
            </button>
            <button
              onClick={async () => {
                let result = await axios({
                  url: `http://localhost:8000/web-users/${item._id}`,
                  method: "delete",
                  headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  },
                });
                getUsers();
                toast(result.data.message);
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadAllUser;
