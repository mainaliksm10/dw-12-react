import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const ReadSpecificUser = () => {
  let [user, setUser] = useState({});
  let navigate = useNavigate();

  let params = useParams();
  let id = params.id;

  let token = localStorage.getItem("token");

  const getAdminUser = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/${id}`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setUser(result.data.data);
    } catch (error) {}
  };

  useEffect(() => {
    getAdminUser();
  }, []);

  return (
    <div>
      <p>Full name : {user.fullName}</p>
      <p>E-mail : {user.email}</p>
      <p>gender : {user.gender}</p>
      <p>Phone Number : {user.phoneNumber}</p>
      <p>Date of Birth : {new Date(user.dob).toLocaleString()}</p>
      <p>Role : {user.role}</p>

      <button
        onClick={() => {
          navigate("/admin/profile-update");
        }}
      >
        Update User
      </button>
    </div>
  );
};

export default ReadSpecificUser;
