import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const AdminProfile = () => {
  let [profile, setProfile] = useState({});

  let navigate = useNavigate();

  let token = localStorage.getItem("token");

  const getAdminProfile = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/my-profile`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setProfile(result.data.data);
    } catch (error) {}
  };

  useEffect(() => {
    getAdminProfile();
  }, []);

  return (
    <div>
      <p>Full name : {profile.fullName}</p>
      <p>E-mail : {profile.email}</p>
      <p>gender : {profile.gender}</p>
      <p>Phone Number : {profile.phoneNumber}</p>
      <p>Date of Birth : {new Date(profile.dob).toLocaleString()}</p>
      <p>Role : {profile.role}</p>

      <button
        onClick={() => {
          navigate("/admin/profile-update");
        }}
      >
        Update Profile
      </button>
    </div>
  );
};

export default AdminProfile;
