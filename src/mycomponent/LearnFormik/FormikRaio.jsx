import { Field } from "formik";
import React from "react";

const FormikRaio = ({ name, label, onChange, options, required, ...props }) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div>
              <label htmlFor={name}>
                {label}
                {""}
                {required ? <span style={{ color: "red" }}>*</span> : null}{" "}
              </label>

              {options.map((item, i) => {
                return (
                  <span>
                    <label htmlFor={item.value}>{item.label}</label>
                    <input
                      {...field}
                      {...props}
                      type="radio"
                      value={item.value}
                      onChange={!!onChange ? onChange : field.onChange}
                      checked={item.value === meta.value}
                    ></input>
                    {meta.touched && meta.error ? (
                      <div style={{ color: "red" }}> {meta.error}</div>
                    ) : null}
                  </span>
                );
              })}
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikRaio;
