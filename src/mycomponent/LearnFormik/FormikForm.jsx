import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikCheckBox from "./FormikCheckBox";
import FormikInput from "./FormikInput";
import FormikRaio from "./FormikRaio";
import FormikSelect from "./FormikSelect";
import FormikTextArea from "./FormikTextArea";

const FormikForm = () => {
  let initialValues = {
    firstName: "",
    lastName: "",
    description: "",
    country: "",
    gender: "",
    isMarried: false,
  };

  let onSubmit = (value, other) => {
    console.log(value);
  };

  let validationSchema = yup.object({
    firstName: yup.string().required("* First Name field is Required"),
    lastName: yup.string().required("* Last Name is required"),
    description: yup.string().required("* Description is required"),
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="firstName"
                label="First Name:    "
                type="text"
                placeholder="First Name"
                required="true"
                style={{ backgroundColor: "orange", color: "green" }}
                onChange={(e) => {
                  formik.setFieldValue("firstName", e.target.value);
                }}
              ></FormikInput>

              <FormikInput
                name="lastName"
                placeholder="Last Name"
                label="Last Name:    "
                type="text"
                required="true"
                style={{ color: "green" }}
                onChange={(e) => {
                  formik.setFieldValue("lastName", e.target.value);
                }}
              ></FormikInput>

              <FormikTextArea
                name="description"
                placeholder="Description"
                label="Description:    "
                type="text"
                style={{ color: "green" }}
                onChange={(e) => {
                  formik.setFieldValue("description", e.target.value);
                }}
              ></FormikTextArea>

              <FormikSelect
                name="country"
                label="Country:    "
                required="true"
                style={{ color: "green" }}
                onChange={(e) => {
                  formik.setFieldValue("country", e.target.value);
                }}
                options={[
                  { label: "Select Country", value: "", disabled: true },
                  {
                    label: "Nepal",
                    value: "nep",
                  },
                  {
                    label: "India",
                    value: "ind",
                  },
                  {
                    label: "China",
                    value: "chi",
                  },
                  {
                    label: "Japan",
                    value: "jap",
                  },
                  {
                    label: "United States Of America",
                    value: "USA",
                  },
                ]}
              ></FormikSelect>

              <FormikRaio
                name="gender"
                label="Gender:    "
                required="true"
                type="radio"
                style={{ color: "green" }}
                onChange={(e) => {
                  formik.setFieldValue("gender", e.target.value);
                }}
                options={[
                  {
                    label: "Male",
                    value: "male",
                  },
                  {
                    label: "Female",
                    value: "female",
                  },
                  {
                    label: "Other",
                    value: "other",
                  },
                ]}
              ></FormikRaio>

              <FormikCheckBox
                name="isMarried"
                label="Is Married?    "
                style={{ color: "green" }}
                type="checkbox"
                onChange={(e) => {
                  formik.setFieldValue("isMarried", e.target.checked);
                }}
              ></FormikCheckBox>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikForm;
