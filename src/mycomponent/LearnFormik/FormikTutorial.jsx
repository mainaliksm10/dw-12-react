import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikRaio from "./FormikRaio";
import FormikSelect from "./FormikSelect";
import FormikCheckBox from "./FormikCheckBox";
import FormikTextArea from "./FormikTextArea";

const FormikTutorial = () => {
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    gender: "male",
    country: "nepal",
    isMarried: false,
    description: "",
    phoneNumber: "",
    age: 0,
  };

  let onSubmit = (value, other) => {
    console.log(value);
  };

  let validationSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required.")
      .min(10, "Must contain at least 10 characters.")
      .max(20, "Must contain at most 20 characters")
      .matches(
        /^[A-Za-z]|[A-Za-z][A-Za-z\s]*[A-Za-z]$/,
        "Should only contain alphabet and space."
      ),
    email: yup
      .string()
      .required("Email is required.")
      .matches(
        /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
        "Email is not Valid"
      ),
    password: yup
      .string()
      .required("Password is required.")
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
        "Password must contain minimum eight characters, at least one letter and one number"
      ),
    gender: yup.string().required("Gender is required."),
    country: yup.string().required("Country is required."),
    isMarried: yup.boolean(),
    description: yup.string(),
    phoneNumber: yup
      .string()
      .required("Phone Number is required.")
    //   .min(10, "Must contain at least 10 characters.")
    //   .max("Must contain at most 10 characters.")
      .matches(
        /^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
        "Invalid Phone Number."
      ),
    age: yup
      .number()
      .required("Age is required.")
      .min(18, "Your age must be above 17, kid!"),
  });

  let genderOptions = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  let countryOptions = [
    {
      label: "Select Country",
      value: "",
      disabled: true,
    },
    {
      label: "Nepal",
      value: "nepal",
    },
    {
      label: "India",
      value: "india",
    },
    {
      label: "Pakistan",
      value: "pakistan",
    },
    {
      label: "China",
      value: "china",
    },
    {
      label: "Korea",
      value: "korea",
    },
    {
      label: "Japan",
      value: "japan",
    },
  ];

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="fullName"
                label="Full Name: "
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("fullName", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="email"
                label="Email: "
                type="email"
                onChange={(e) => {
                  formik.setFieldValue("email", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="password"
                label="Password: "
                type="password"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("password", e.target.value);
                }}
              ></FormikInput>

              <FormikRaio
                name="gender"
                label="Gender:  "
                onChange={(e) => {
                  formik.setFieldValue("gender", e.target.value);
                }}
                required={true}
                options={genderOptions}
              ></FormikRaio>

              <FormikSelect
                name="country"
                label="Country: "
                onChange={(e) => {
                  formik.setFieldValue("country", e.target.value);
                }}
                required={true}
                options={countryOptions}
              ></FormikSelect>

              <FormikCheckBox
                name="isMarried"
                label="Is Married?    "
                onChange={(e) => {
                  formik.setFieldValue("isMarried", e.target.checkbox);
                }}
              ></FormikCheckBox>

              <FormikTextArea
                name="description"
                label="Description"
                type="textarea"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("description", e.target.value);
                }}
              ></FormikTextArea>

              <FormikInput
                name="phoneNumber"
                label="Phone Number: "
                type="string"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("phoneNumber", e.target.value);
                }}
              ></FormikInput>
 
              <FormikInput
                name="age"
                label="Age"
                type="number"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("age", e.target.value);
                }}
              ></FormikInput>

              <br></br>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikTutorial;
