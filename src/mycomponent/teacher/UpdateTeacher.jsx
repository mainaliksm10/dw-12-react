import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UpdateTeacher = () => {
  let [name, setName] = useState("");
  let [address, setAddress] = useState("");
  let params = useParams();
  let navigate = useNavigate();

  const getTeacher = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/teachers/${params.id}`,
        method: "get",
      });
      console.log(result);
      setName(result.data.result.name);
      setAddress(result.data.result.address);
      //   setTeacher(result.data.result);
    } catch (error) {}
  };

  useEffect(() => {
    getTeacher();
  }, []);

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      address: address,
    };
    // console.log(data);

    try {
      let result = await axios({
        url: `http://localhost:8000/teachers/${params.id}`,
        method: "patch",
        data: data,
      });

      //   setName("");
      //   setAddress("");
      //   toast(result.data.message);
      navigate("/teachers");
    } catch (error) {
      // console.log(error.message);
      toast(error.response.data.message);
    }
  };
  return (
    <div>
      <ToastContainer />
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address: </label>
          <input
            type="text"
            id="address"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>
        <button type="submit">Update</button>
      </form>
    </div>
  );
};

export default UpdateTeacher;
