import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const CreateTeacher = () => {
  let [name, setName] = useState("");
  let [address, setAddress] = useState("");

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      address: address,
    };
    // console.log(data);

    try {
      let result = await axios({
        url: `http://localhost:8000/teachers`,
        method: "post",
        data: data,
      });

      setName("");
      setAddress("");
      toast(result.data.message);
    } catch (error) {
      // console.log(error.message);
      toast(error.response.data.message);
    }
  };
  return (
    <div>
      <ToastContainer />
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address: </label>
          <input
            type="text"
            id="address"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>
        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default CreateTeacher;
