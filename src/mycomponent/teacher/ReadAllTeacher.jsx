import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ReadAllTeacher = () => {
  let [teachers, setTeachers] = useState([]);
  let navigate = useNavigate();

  let getTeachers = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/teachers",
        method: "get",
      });
      console.log(result);
      setTeachers(result.data.result);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getTeachers();
  }, []);
  return (
    <div>
      <ToastContainer />
      {teachers.map((item, i) => {
        return (
          <div
            key={i}
            style={{
              border: "solid 10px orange",
              marginBottom: "5px",
              paddingLeft: "5px",
            }}
          >
            <p>Name is {item.name}</p>
            {/* <p>Address is {item.address}</p> */}
            <button
              onClick={() => {
                navigate(`/teachers/${item._id}`);
              }}
            >
              View
            </button>
            <button
              onClick={() => {
                navigate(`/teachers/update/${item._id}`);
              }}
            >
              Edit
            </button>
            <button
              onClick={async () => {
                let result = await axios({
                  url: `http://localhost:8000/teachers/${item._id}`,
                  method: "delete",
                });
                getTeachers();
                toast(result.data.message);
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadAllTeacher;
